//
//  Item.swift
//  Shop
//
//  Created by Astrear on 1/19/19.
//  Copyright © 2019 Dark. All rights reserved.
//

import Foundation
struct Item {
    let id: Int
    let name: String
    let description: String
    let image: String
    let price: Int
}
