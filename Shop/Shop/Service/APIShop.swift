//
//  APIShop.swift
//  Shop
//
//  Created by Astrear on 1/19/19.
//  Copyright © 2019 Dark. All rights reserved.
//
import Foundation

class APIShop {
    
    private var baseUrl: String
    
    init(url: String) {
        self.baseUrl = url
    }
    
    func request(endpoint: String, body: Data, method: String, callback: @escaping (_ data: [String: Any]?) -> Void) {
        
        let url = URL(string: "\(baseUrl)\(endpoint)")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = method
        request.httpBody = body
        
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print("error=\(error!)")
                callback(errors.NetworkError)
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response!)")
                callback(errors.RequestError)
            }
            
            do {
                let response = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                callback(response)
            } catch let error {
                print(error)
                callback(errors.ParseError)
            }
        }.resume()
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    struct errors {
        static let NetworkError: [String: Any] = ["code": 0, "message": "There was a network error."]
        static let RequestError: [String: Any] = ["code": 1, "message": "There was a request error."]
        static let ParseError: [String: Any] = ["code": 2, "message": "There was a error parsing the response data."]
    }
}
