//
//  ViewController.swift
//  Shop
//
//  Created by Astrear on 1/19/19.
//  Copyright © 2019 Dark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        button.layer.cornerRadius = 10
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }


    @IBAction func login(push: UIStoryboardSegue) {
        let api = APIShop(url: "http://127.0.0.1:8000/")
        
        guard let user = username.text, let pass = password.text, !user.isEmpty && !pass.isEmpty else {
            alert(title: "Warning", message: "Fields can't be empty", button: Button(title: "Accept", handler: nil))
            return
        }
        
        let json = ["username": user, "password": pass]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        api.request(endpoint: "api-token-auth/", body: jsonData!, method: "POST", callback: { data in
            if let _ = data?["code"] {
                DispatchQueue.main.async {
                    self.alert(title: "Error", message: data?["message"] as! String, button: Button(title: "Accept", handler: nil))
                }
            } else  {
                if let token = data?["token"] {
                    let defaults = UserDefaults.standard
                    defaults.set(token, forKey: "token")
                    if let data = defaults.string(forKey: "token") {
                        print("token", data)
                    }
                    DispatchQueue.main.async {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "List")
                        self.present(newViewController, animated: true, completion: nil)
                    }
                }
            }
        } )
        
    }
    
    func alert(title: String, message: String, button: Button) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: button.title, style: UIAlertAction.Style.default, handler: button.handler))
        self.present(alert, animated: true, completion: nil)
    }
}

struct Button {
    let title: String
    let handler: ((UIAlertAction)-> Void)?
}
